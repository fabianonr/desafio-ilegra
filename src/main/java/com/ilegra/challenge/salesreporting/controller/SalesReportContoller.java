package com.ilegra.challenge.salesreporting.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.ilegra.challenge.salesreporting.service.DataFile;
import com.ilegra.challenge.salesreporting.service.FileExport;
import com.ilegra.challenge.salesreporting.service.FileImporter;

public class SalesReportContoller implements Runnable{
	private FileImporter importer;
	private FileExport exporter;
	
	public SalesReportContoller( FileImporter importer, FileExport exporter ){
		this.importer = importer;
		this.exporter = exporter;
	}

	@Override
	public void run() {
		try {
			
			System.out.println( "Executando.." );
			execute();
			System.out.println( "Parando.." );
			
		} catch (Exception e) {
			System.out.println( e.getMessage() );
		}
	}

	public void exportAndRemoveFileImporte( DataFile file ) throws IOException{
		exporter.export( file );
		Files.delete( Paths.get( importer.getPath(), "/", file.getName() ) );
	}
	
	private void execute() throws Exception{
		List<DataFile> files = importer.importFile();
		for( DataFile file : files )
			exportAndRemoveFileImporte( file );		
	}
}
