package com.ilegra.challenge.salesreporting.rule;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ilegra.challenge.salesreporting.entity.Sale;
import com.ilegra.challenge.salesreporting.service.DataFile;
import com.ilegra.challenge.salesreporting.service.DataFileAnalyzer;

public class SalesReportAnalyzer implements DataFileAnalyzer {

	private static final String REGEX_SALESMAN_ID = "001";
	private static final String REGEX_CUSTUMER_ID = "002";
	private static final String REGEX_SALES_ID = "003";
	private static final String REGEX_SALE_ID = "\\d{2}";
	private static final String REGEX_DELIMITER = "ç";
	private static final String REGEX_CPF = "\\d{13}";
	private static final String REGEX_CNPJ = "\\d{16}";
	private static final String REGEX_NAME = "[A-Z][a-z]*";
	private static final String REGEX_FULLNAME = "([A-Z][a-z]*)((\\s[a-z]{2})?(\\s[A-Z][a-z]*))*";
	private static final String REGEX_SALARY = "\\d[0-9]*+(\\.[0-9]{2}+)?";//$";
	private static final String REGEX_BUSINESS_AREA = REGEX_NAME;
	private static final String REGEX_ITEM = "\\d[0-9]*\\-\\d[0-9]*\\-"+ REGEX_SALARY;
	private static final String REGEX_LIST_ITEM = "\\["+ REGEX_ITEM +"(,"+ REGEX_ITEM +")*\\]";
	
	private static String salesmanPattern;
	private static String custumerPattern;
	private static String salesPattern;
	
	public SalesReportAnalyzer(){
		salesmanPattern = REGEX_SALESMAN_ID + REGEX_DELIMITER + 
				REGEX_CPF + REGEX_DELIMITER + 
				REGEX_NAME + REGEX_DELIMITER + 
				REGEX_SALARY;
		
		custumerPattern = REGEX_CUSTUMER_ID + REGEX_DELIMITER + 
				REGEX_CNPJ + REGEX_DELIMITER + 
				REGEX_FULLNAME + REGEX_DELIMITER + 
				REGEX_BUSINESS_AREA;
		
		salesPattern = REGEX_SALES_ID + REGEX_DELIMITER +
				REGEX_SALE_ID + REGEX_DELIMITER +
				REGEX_LIST_ITEM + REGEX_DELIMITER +
				REGEX_NAME;
	}
	
	public boolean isSalesmanPattern(String rowData ) {		
		return isMatch( salesmanPattern, rowData );
	}

	public boolean isCustumerPattern( String rowData ) {
		return isMatch( custumerPattern, rowData );
	}

	public boolean isSalesPattern(String rowData ) {
		return isMatch( salesPattern, rowData );
	}
	
	public boolean isSalesItemListPattern(String rowData ) {
		String salesItemList = REGEX_LIST_ITEM;
		return isMatch( salesItemList , rowData );
	}

	public DataFile parse( List<String> rowsOfFile ) throws Exception {
		SalesReport report = new SalesReport( "sales_report_file" );
		
		int index = 1;
		for( String rowData : rowsOfFile )
		{
			if( isSalesmanPattern( rowData ) )
				report.addSalesman( rowData );
			else if( isCustumerPattern( rowData ) )
				report.addCustumer( rowData );
			else if( isSalesPattern( rowData ) )
				report.addSale( Sale.parse( rowData ) );
			else
				throw new Exception( "Há um arquivo inválido na linha " + index );
			
			index++;
		}
		
		return report;
	}

	private boolean isMatch( String regex, String rowData ){
		Pattern pattern = Pattern.compile( regex );
		Matcher matcher = pattern.matcher( rowData );
		return matcher.matches();
	}


}
