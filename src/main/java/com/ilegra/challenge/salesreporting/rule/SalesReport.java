package com.ilegra.challenge.salesreporting.rule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ilegra.challenge.salesreporting.entity.Sale;
import com.ilegra.challenge.salesreporting.service.DataFile;

public class SalesReport extends DataFile{
	private List<String> salesmanList;
	private List<String> custumerList;	
	private Sale theMostExpensiveSale;
	private Map<String, Double> salesMapBySalesman;
	
	public SalesReport( String name ){
		super( name );
		custumerList = new ArrayList<String>();
		salesmanList = new ArrayList<String>();
		theMostExpensiveSale = new Sale();
		salesMapBySalesman = new HashMap<String, Double>();
	}

	public List<String> getData() {
		List<String> data = new ArrayList<>();
		data.add( "Amount of clients in the input file = " + custumerList.size() );
		data.add( "Amount of salesman in the input file = " + salesmanList.size() );
		data.add( "ID of the most expensive sale = " + theMostExpensiveSale.getId() );
		data.add( "Worst salesman ever = " + getWorsSalesmanEver() );

		return data;
	}

	public void addSalesman( String rowData ) {
		salesmanList.add( rowData );
	}

	public void addCustumer(String rowData) {
		custumerList.add( rowData );
	}

	public void addSale( Sale sale ) {
		calculateIdOfTheMostExpensiveSale( sale );
		addSalesBySalesman( sale );
	}
	
	private void addSalesBySalesman(Sale sale) {
		Double salesValue = salesMapBySalesman.getOrDefault( sale.getSalesman(), 0.0 );
		salesMapBySalesman.put( sale.getSalesman(), sale.getSalesValue()+salesValue );
	}

	public void calculateIdOfTheMostExpensiveSale( Sale sale ){
		if( sale.isHiggerThan( theMostExpensiveSale ) )
			theMostExpensiveSale = sale;
	}
	
	private String getWorsSalesmanEver(){
		
		Double worstSalesmanEver = Collections.min(salesMapBySalesman.values() );
		return salesMapBySalesman.entrySet().stream()
				.filter( e-> e.getValue().equals( worstSalesmanEver ) )
				.findFirst().get().getKey();
	}
}
