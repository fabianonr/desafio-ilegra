package com.ilegra.challenge.salesreporting.entity;

import java.util.ArrayList;
import java.util.List;

public class Sale {
	private Long id;
	private String salesman;
	private List<ItemSale> items;
	private Double mySalesValue;
	
	public Sale(){
		mySalesValue = 0.0;
		items = new ArrayList<ItemSale>();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSalesman() {
		return salesman;
	}

	public void setSalesman(String salesman) {
		this.salesman = salesman;
	}

	public List<ItemSale> getItems() {
		return items;
	}

	public void setItems(List<ItemSale> items) {
		this.items = items;
	}

	public boolean isHiggerThan( Sale theMostExpensiveSale ) {
		return getSalesValue() > theMostExpensiveSale.getSalesValue();
	}

	public Double getSalesValue() {
		if( mySalesValue.equals( 0.0 ) )
			calculateSale();
		
		return mySalesValue;
	}
	
	private void calculateSale(){
		for( ItemSale item : items )
			mySalesValue += item.getSalesValue();
	}

	public static Sale parse( String rowData ) {
		String[] data = rowData.split( "ç" );
		
		Sale sale = new Sale();

		if( data[0].equals( "003" ) )
		{
			sale.setId( Long.parseLong( data[1] ) );
			sale.setSalesman( data[3] );
			
			String itemList = data[2].substring(1, data[2].length()-1 );
			String[] items = itemList.split( "," );
			
			for( String item : items )
				sale.addItem( ItemSale.parse( item ) );
		}
		
		return sale;
	}

	private void addItem(ItemSale item ) {
		items.add( item );
	}
}
