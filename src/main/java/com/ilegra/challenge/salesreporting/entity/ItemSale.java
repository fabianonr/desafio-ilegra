package com.ilegra.challenge.salesreporting.entity;

public class ItemSale {
	private Long id;
	private Integer quantity;
	private Double price;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getSalesValue() {
		return quantity * price;
	}
	
	public static ItemSale parse(String rowData ) {
		String[] data = rowData.split( "-" );
		
		ItemSale item = new ItemSale();
		item.setId( Long.parseLong( data[0] ) );
		item.setQuantity( Integer.parseInt( data[1] ) );
		item.setPrice( Double.parseDouble( data[2] ) );
		
		return item;
	}
}
