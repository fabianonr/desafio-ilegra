package com.ilegra.challenge.salesreporting.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileImporter extends FileIO{
	DataFileAnalyzer analyzer;

	public FileImporter( DataFileAnalyzer analyzer, FileIOConfig config ){
		super( config );
		this.analyzer = analyzer;
	}

	public List<DataFile> importFile() throws Exception {
		List<DataFile> files = new ArrayList<DataFile>();
		List<String> filesName = getOnlyFilesNameWithCorrectExtension();
		
		for( String filename : filesName ){
			DataFile file = buildFile( filename );
			files.add( file );			
		}

		return files;
	}

	public List<String> importData( String fileName ) throws Exception{

		try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {

			Stream<String> stream = br.lines();
			return stream.collect(Collectors.toList() );
		} catch (IOException e) {
			throw new Exception( "Falha na tentativa de importar o arquivo " + fileName + "." );
		}
	}

	private List<String> getOnlyFilesNameWithCorrectExtension() throws Exception{
		final List<String> fileNameList = new ArrayList<>();

		findFilesWithExtension()
		.forEach( objectPath -> fileNameList.add( objectPath.getFileName().toString() ) );

		return fileNameList;
	}

	private DirectoryStream<Path> findFilesWithExtension() throws Exception{
		try {
			return Files.newDirectoryStream( Paths.get( path ),
					filePath -> filePath.toString()
					.endsWith( extension ) );
		} catch (IOException e) {
			throw new Exception( "Não foi possivel importar arquivos com a extensão " + extension +
					"do diretório " + path );
		}
	}

	private DataFile buildFile( String filename ) throws Exception{
		String fullFilename = path + "/" + filename;
		List<String> data = importData( fullFilename );
		DataFile file = analyzer.parse( data ); 
		file.setName( filename );
		return file;
	}
}
