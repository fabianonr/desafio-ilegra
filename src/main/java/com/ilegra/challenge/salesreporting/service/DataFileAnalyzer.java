package com.ilegra.challenge.salesreporting.service;

import java.util.List;

public interface DataFileAnalyzer {
	public DataFile parse( List<String> data ) throws Exception;
}
