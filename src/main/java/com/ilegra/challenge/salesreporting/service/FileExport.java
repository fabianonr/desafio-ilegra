package com.ilegra.challenge.salesreporting.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileExport extends FileIO{
		
	public FileExport( FileIOConfig config ){
		super( config );
	}
	
	public void export( DataFile file ) throws IOException{
		StringBuilder rowDataOfFile = new StringBuilder();
		file.getData().forEach( row -> rowDataOfFile.append( row ).append( "\n" ) );
		
		write( rowDataOfFile, file.getName() );
	}
	
	public void write( StringBuilder text, String filename ) throws IOException{
		int index = filename.indexOf(".");
		
		String filenameWithoutExtension = filename.substring( 0, index );
		
		Path objectPath = Paths.get( path + "/" + filenameWithoutExtension + extension );
		 
		try (BufferedWriter writer = Files.newBufferedWriter( objectPath ) ) 
		{
		    writer.write( text.toString() );
		}
	}
}
