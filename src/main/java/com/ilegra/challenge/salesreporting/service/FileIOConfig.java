package com.ilegra.challenge.salesreporting.service;

public class FileIOConfig {
	String enviromentVariableName;
	String complementPath;
	String fileExtension;
	
	public String getEnviromentVariableName() {
		return enviromentVariableName;
	}
	public void setEnviromentVariableName(String enviromentVariableName) {
		this.enviromentVariableName = enviromentVariableName;
	}
	public String getComplementPath() {
		return complementPath;
	}
	public void setComplementPath(String complementPath) {
		this.complementPath = complementPath;
	}
	public String getFileExtension() {
		return fileExtension;
	}
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}
}
