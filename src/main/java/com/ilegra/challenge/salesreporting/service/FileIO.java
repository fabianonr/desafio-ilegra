package com.ilegra.challenge.salesreporting.service;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

public abstract class FileIO {
	protected String enviromentVariableName;
	protected String path;
	protected String extension;
	
	public FileIO( FileIOConfig config ){
		this.enviromentVariableName = config.getEnviromentVariableName();
		this.extension = config.getFileExtension();
		setFullPath( config.getComplementPath() );
	}
	
	private void setFullPath( String partialPath ){		
		
		Optional<Entry<String,String>> variable = findVariable();
		
		if( variable.isPresent() )
			path = variable.get().getValue() + partialPath;
		else
			path = partialPath;
	}
	
	private Optional<Entry<String,String>> findVariable(){
		Map<String,String> variableEnvironmentMap = System.getenv();
		
		Optional<Entry<String,String>> result =
				variableEnvironmentMap.entrySet()
				.stream()
				.filter(variable -> variable.getKey().equals( enviromentVariableName ) )
				.findFirst();
		
		return result;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
