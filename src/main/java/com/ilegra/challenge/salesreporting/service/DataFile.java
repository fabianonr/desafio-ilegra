package com.ilegra.challenge.salesreporting.service;

import java.util.List;

public abstract class DataFile {
	private String name;
	
	public DataFile( String name ){
		this.name = name;
	}
	
	public abstract List<String> getData();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
