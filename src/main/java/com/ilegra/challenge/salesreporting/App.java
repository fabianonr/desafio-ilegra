package com.ilegra.challenge.salesreporting;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.ilegra.challenge.salesreporting.controller.SalesReportContoller;
import com.ilegra.challenge.salesreporting.rule.SalesReportAnalyzer;
import com.ilegra.challenge.salesreporting.service.FileExport;
import com.ilegra.challenge.salesreporting.service.FileIOConfig;
import com.ilegra.challenge.salesreporting.service.FileImporter;

/**
 * Hello world!
 *
 */
public class App 
{
	FileImporter importer;
	FileExport exporter;
	Runnable controller;

	public static void main( String[] args )
	{
		App app = new App();

		app.run();
	}

	public App(){
		FileIOConfig importerConfig = getImporterConfig();
		FileIOConfig exporterConfig = getExporterConfig();
		importer = createImporter( importerConfig );
		exporter = createExporter( exporterConfig );
		controller = createController();
	}

	private Runnable createController() {
		return new SalesReportContoller( importer, exporter );
	}

	public void run(){
		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate( controller, 1, 4, TimeUnit.SECONDS );
	}

	public FileIOConfig getImporterConfig(){
		FileIOConfig config = new FileIOConfig();
		config.setEnviromentVariableName( "HOMEPATH" );
		config.setComplementPath( "/data/in" );
		config.setFileExtension( ".dat" );
		return config;
	}

	public FileIOConfig getExporterConfig(){
		FileIOConfig config = new FileIOConfig();
		config.setEnviromentVariableName( "HOMEPATH" );
		config.setComplementPath( "/data/out" );
		config.setFileExtension( ".done.dat" );
		return config;
	}

	public FileImporter createImporter( FileIOConfig config ){
		return new FileImporter( new SalesReportAnalyzer(), config );
	}

	public FileExport createExporter( FileIOConfig config ) {
		return new FileExport( config );
	}
}
