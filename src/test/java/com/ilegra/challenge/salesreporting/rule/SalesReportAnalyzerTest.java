package com.ilegra.challenge.salesreporting.rule;

import java.util.ArrayList;
import java.util.List;

import com.ilegra.challenge.salesreporting.entity.Sale;
import com.ilegra.challenge.salesreporting.service.DataFile;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class SalesReportAnalyzerTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SalesReportAnalyzerTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( SalesReportAnalyzerTest.class );
    }

    public void testCorrectReportGeneration() throws Exception{
    	List<String> rowsOfFile = new ArrayList<String>();
    	rowsOfFile.add("001ç1234567891234çDiegoç50000");
    	rowsOfFile.add("001ç3245678865434çRenatoç40000.99");
    	rowsOfFile.add("002ç2345675434544345çJose da SilvaçRural");
    	rowsOfFile.add("002ç2345675433444345çEduardo PereiraçRural");
    	rowsOfFile.add("003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego");
    	rowsOfFile.add("003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato");

    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	DataFile reportGenerated = analyzer.parse( rowsOfFile );
    	
    	SalesReport reportExpected = new SalesReport("teste");
    	reportExpected.addSalesman( "001ç1234567891234çDiegoç50000" );
    	reportExpected.addSalesman( "001ç3245678865434çRenatoç40000.99" );
    	reportExpected.addCustumer( "002ç2345675434544345çJose da SilvaçRural" );
    	reportExpected.addCustumer( "002ç2345675433444345çEduardo PereiraçRural" );
    	reportExpected.addSale( Sale.parse( "003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego" ) );
    	reportExpected.addSale( Sale.parse( "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato" ) );
    	
    	assertEquals( reportExpected.getData(), reportGenerated.getData() );
    }

    public void testRecognitionSalesmanPattern()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertTrue(analyzer.isSalesmanPattern("001ç1234567891234çDiegoç5000") );
    	assertTrue(analyzer.isSalesmanPattern("001ç3245678865434çRenatoç40000.99") );
    	assertTrue(analyzer.isSalesmanPattern("001ç0139214789233çFabianoç4") );
    }
    
    public void testRejectionSalesmanWrongId()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesmanPattern("002ç1234567891234çDiegoç5000" ) ) ;
    }
    
    public void testRejectionSalesmanWithoutCpf()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesmanPattern("001ççDiegoç5000") );
    }
    
    public void testRejectionSalesmanWithoutSalay()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesmanPattern("001ç1234567891234çDiegoç") );
    }
    
    public void testRejectionSalesmanWithoutDelimiter()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesmanPattern("0011234567891234Diego") );
    }
    
    public void testRejectionSalesmanWithoutOneDelimiter()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesmanPattern("001ç3245678865434Renatoç40000.99") );
    }
    
    public void testRejectionSalesmanWithoutName()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesmanPattern("001ç1234567891234çç5000") );
    }
    
    public void testRejectionSalesmanWrongName()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesmanPattern("001ç3245678865434çrenatoç40000.99") );
    }
    
    public void testRecognitionCustumerPattern()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertTrue(analyzer.isCustumerPattern("002ç2345675434544345çJose da SilvaçRural") );
    	assertTrue(analyzer.isCustumerPattern("002ç2345675433444345çEduardo PereiraçRural") );
    }
    
    public void testRejectionCustumerWrongId()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isCustumerPattern("003ç2345675434544345çJose da SilvaçRural") );
    }
    
    public void testRejectionCustumerWithoutCnpj()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesmanPattern("002ççJose da SilvaçRural") );
    }
    
    public void testRejectionCustumerWithoutBusinessArea()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isCustumerPattern("002ç2345675433444345çEduardo Pereiraç") );
    }
    
    public void testRejectionCustumerWithoutId()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isCustumerPattern("ç2345675433444345çEduardo PereiraçRural") );
    }
    
    public void testRejectionCustumerWithoutName()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isCustumerPattern("002ç2345675433444345ççRural") );
    }
    
    public void testRejectionCustumerWithoutDelimiter()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertTrue(analyzer.isCustumerPattern("002ç2345675433444345çEduardo PereiraçRural") );
    }
    
    public void testRejectionCustumerWrongName()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isCustumerPattern("002ç2345675433444345çEduardoPereiraçRural") );
    }
    
    public void testRecognitionSalesPattern()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertTrue(analyzer.isSalesPattern("003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego") );
    	assertTrue(analyzer.isSalesPattern("003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato") );
    	assertTrue(analyzer.isSalesPattern("003ç29ç[7-5-174,8-37-12.00,6-1040-7589.10]çJuliano") );
    }
    
    public void testRejectionSalesWrongId()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesPattern("001ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego") );
    }
    
    public void testRejectionSalesListEmpty()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesPattern("003ç10ç[]çDiego") );
    }
    
    public void testRejectionSalesWithoutSalesman()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesPattern("003ç08ç[1-34-10,2-33-1.50,3-40-0.10]ç") );
    }
    
    public void testRejectionSalesWithoutSalesId()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertFalse(analyzer.isSalesPattern("003çç[1-34-10,2-33-1.50,3-40-0.10]çRenato") );
    }
    
    public void testRecognitionJustSalesItemListPattern()
    {
    	SalesReportAnalyzer analyzer = new SalesReportAnalyzer();
    	assertTrue(analyzer.isSalesItemListPattern("[1-10-100,1-10-100]") );
    }
    
}
