package com.ilegra.challenge.salesreporting.service;

import java.util.List;

import com.ilegra.challenge.salesreporting.entity.Sale;
import com.ilegra.challenge.salesreporting.rule.SalesReport;
import com.ilegra.challenge.salesreporting.rule.SalesReportAnalyzer;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class FileImporterTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FileImporterTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( FileImporterTest.class );
    }

    /**
     * Rigourous Test :-)
     * @throws Exception 
     */
    
    public void testIfItCanReadFromEnvironmentVariable() throws Exception{
    	
    	FileIOConfig config = new FileIOConfig();
    	config.setFileExtension(".dat");
    	config.setEnviromentVariableName("HOMEPATH");
    	config.setComplementPath("/data/teste");
    	
    	FileImporter reader = new FileImporter( new SalesReportAnalyzer(), config );
    	List<DataFile> reportImported = reader.importFile();
    	    	
    	SalesReport reportExpected = new SalesReport("teste.dat");
    	reportExpected.addSalesman( "001ç1234567891234çDiegoç50000" );
    	reportExpected.addSalesman( "001ç3245678865434çRenatoç40000.99" );
    	reportExpected.addCustumer( "002ç2345675434544345çJose da SilvaçRural" );
    	reportExpected.addCustumer( "002ç2345675433444345çEduardo PereiraçRural" );
    	reportExpected.addSale( Sale.parse( "003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego" ) );
    	reportExpected.addSale( Sale.parse( "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato" ) );
    	
        assertEquals( reportExpected.getData(), reportImported.get( 0 ).getData() );
    }
}
