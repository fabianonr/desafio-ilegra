package com.ilegra.challenge.salesreporting.service;

import java.io.IOException;

import com.ilegra.challenge.salesreporting.entity.Sale;
import com.ilegra.challenge.salesreporting.rule.SalesReport;
import com.ilegra.challenge.salesreporting.service.FileExport;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class FileExporterTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FileExporterTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( FileExporterTest.class );
    }

    /**
     * Rigourous Test :-)
     * @throws Exception 
     */
    
    public void testWriteDirectoryFromEnvironmentVariable(){
    	FileIOConfig config = new FileIOConfig();
    	config.setFileExtension(".done.dat");
    	config.setEnviromentVariableName("HOMEPATH");
    	config.setComplementPath("/data/out");
    	FileExport fileExporter = new FileExport( config );
    	
    	SalesReport reportExpected = new SalesReport("teste.dat");
    	reportExpected.addSalesman( "001ç1234567891234çDiegoç50000" );
    	reportExpected.addSalesman( "001ç3245678865434çRenatoç40000.99" );
    	reportExpected.addCustumer( "002ç2345675434544345çJose da SilvaçRural" );
    	reportExpected.addCustumer( "002ç2345675433444345çEduardo PereiraçRural" );
    	reportExpected.addSale( Sale.parse( "003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego" ) );
    	reportExpected.addSale( Sale.parse( "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato" ) );
    	
    	try {
			fileExporter.export( reportExpected );
			assertTrue( true );
		} catch (IOException e) {
			fail();
			e.printStackTrace();
		}
    }
}
