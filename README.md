# salesreporting

## Requisitos
- jre1.8.0_111 ou superior

## Instalação
- Criar diretório data/in e data/out a partir da variável de ambiente %HOMEPATH%;
- No Windows, via linha de comando, executar o arquivo .jar que está localizado em salesreporting/target com o seguinte comando: 
  java -jar salesreporting-jar-with-dependencies
  
  ## Tecnologia utilizada
  - Java
  - Junit
  
  ## Destaques da lógica
  - O código foi desenvolvido observando-se princípios e boas práticas de programação, tais como: Princípio de responsabilidade única, baixo acoplamento, código limpo e práticas TDD;
  
  ## O que poderia ter sido diferente
   - As expressões regulares usadas na classe SalesReportAnalyzer ficariam melhor se estivessem em uma outra classe que fosse responsável por fazer o parse das entidades, caso das entidades Sale e ItemSale. Além de tornar a classe SalesReportAnalyzer mais leve livraria as entidades de terem que conhecer algumas particularidades do padrão do arquivo de entrada (como o delimitador e o ID).
